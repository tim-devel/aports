# Contributor: Peter Bui <pnutzh4x0r@gmail.com>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Timo Teräs <timo.teras@iki.fi>
# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Sodface <sod@sodface.com>
pkgname=yt-dlp
pkgver=2023.01.02
pkgrel=0
pkgdesc="Command-line program to download videos from YouTube"
url="https://github.com/yt-dlp/yt-dlp"
arch="noarch"
license="Unlicense"
depends="python3 py3-mutagen py3-websockets py3-certifi py3-brotli"
makedepends="py3-setuptools"
checkdepends="py3-flake8 py3-nose py3-pytest"
subpackages="
	$pkgname-doc
	$pkgname-zsh-completion
	$pkgname-bash-completion
	$pkgname-fish-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/yt-dlp/yt-dlp/releases/download/$pkgver/yt-dlp.tar.gz"
builddir="$srcdir/$pkgname"

build() {
	python3 setup.py build

	make completions
}

check() {
	PYTHON=/usr/bin/python3 make offlinetest
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"

	# Install fish completion to the correct directory
	rm -r "$pkgdir"/usr/share/fish/vendor_completions.d
	install -Dm644 completions/fish/yt-dlp.fish \
		-t "$pkgdir"/usr/share/fish/completions
}

sha512sums="
f0b16647f0a67bb29eef92f75820f29ca7dccfbb5685cddd12b0f21940511209d4b49104ba6dbf217a5fbb592c6693c1d578ac4561c23c15e8772f2b9ab6f6fa  yt-dlp-2023.01.02.tar.gz
"
